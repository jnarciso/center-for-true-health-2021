<?php
/**
 * The template for displaying the home/front page.
 *
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-home'); ?>

	<div class="main-content" id="main-content" role="main">

	<?php get_template_part('sections/sessions-grid'); ?>		

	<?php if ( have_rows( 'about_section' ) ) { ?>
		<section class="about-content--home background-gradient--orange">
			<div class="container">
				<div class="row d-flex align-items-center">
    	<?php while ( have_rows( 'about_section' ) ) { the_row();
    		$about_title = get_sub_field( 'about_title' );
    		$about_content = get_sub_field( 'about_content' );
			$about_image = get_sub_field( 'about_image' );
		?>		

					<?php if ( !empty( $about_title ) || !empty( $about_content ) ) { ?>
					<div class="col-lg-6 order-lg-2 text-center text-lg-left">

						<?php if ( !empty( $about_title ) ) { ?>
						<h2><?php esc_html_e( $about_title, 'tgs_wp' ); ?></h2>
						<?php } ?>

						<div class="about-content--inner">
						<?php if ( !empty( $about_content ) ) { 
							echo wp_kses_post( $about_content );
						} ?>

		        		<?php 
		        			// check for buttons
							if ( have_rows( 'about_buttons' ) ) {
								while ( have_rows( 'about_buttons' ) ) { the_row();
	    						$button_text = get_sub_field( 'button_text' );
	    						$button_type = get_sub_field( 'button_type' );
	    						$button_link = ''; 

	    						if ( $button_type == 'internal' ) {
	    							$button_link = get_sub_field( 'button_link_page' );
	    						} else {
	    							$button_link = get_sub_field( 'button_link_url' );
	    						}

	    					?>
	    					<a href="<?php echo esc_url( $button_link ); ?>" class="button orange-light"><?php esc_html_e( $button_text, 'tgs_wp' ); ?></a>
							<?php }
						} ?>
						</div>

					</div>
					<?php } ?>

	        		<?php if ( !empty( $about_image ) ) { ?>
		        	<div class="col-lg-6 order-lg-1 about-image text-center">
						<img src="<?php echo esc_url( $about_image['url'] ); ?>" alt="<?php echo esc_attr( $about_image['alt'] ); ?>" class="img-fluid">
					</div>
		        	<?php } ?>

    	<?php } ?>
    			</div>
    		</div>
		</section>
    <?php } ?>

    <?php 
    	$upcoming_soul_event_query = new WP_Query( array(
			'post_type'      => 'soul_healing_event',
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_status'    => 'publish',
			'posts_per_page' => 1
    ));
    // The Loop
    if ( $upcoming_soul_event_query->have_posts() ) { 
        while ( $upcoming_soul_event_query->have_posts() ) {
        	$upcoming_soul_event_query->the_post(); 
        	$event_intro_blurb = get_field( 'event_intro_blurb' );
    ?>
	<section class="upcoming-soul-healing-section">
		<div class="container">
			<div class="row"> 

				<div class="col-lg-6 upcoming-soul-healing-section--image text-center text-lg-left">
	    			<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/home--weekly-meditation.jpg' ); ?>" alt="Man on couch meditating with headphones on" class="img-fluid">
				</div>

				<div class="col-lg-6 upcoming-soul-healing-section--content text-center text-lg-left">
					<div class="upcoming-soul-healing-section--content-inner">
						<h2><span><?php esc_html_e( 'Upcoming Soul Healing Event', 'tgs_wp' ); ?></span><br><?php the_title(); ?></h2>

						<?php if ( !empty( $event_intro_blurb ) ) {
							echo wp_kses_post( $event_intro_blurb );
						} ?>

						<a href="/soul-healing-events/" class="button"><?php esc_html_e( 'Learn More', 'tgs_wp' ); ?></a>
					</div>
				</div>

			</div>
		</div>
	</section>
    <?php } ?>    

    <?php } ?>

	<?php wp_reset_postdata(); ?>

	</div>

<?php get_footer();
