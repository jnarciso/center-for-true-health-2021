<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and <header> section
 *
 * @package tgs_wp
 */
// check if ACF plugin is installed and active first
if ( class_exists('ACF') ) {
	// get values from our ACF Options/Site Info Page
	if ( have_rows('miscellaneous_options', 'option') ) {
		while ( have_rows('miscellaneous_options', 'option') ) { the_row();		

			if ( get_sub_field( 'menu_type' ) === 'click' ) {
				$dropdown_type = 'click';
			} else {
				$dropdown_type = 'hover';
			}

			if ( get_sub_field( 'google_tag_manager_id' ) ) {
				$gtm_container_id = get_sub_field('google_tag_manager_id');
			}

			if ( get_sub_field( 'facebook_pixel' ) ) {
				$fb_pixel = get_sub_field('facebook_pixel');
			}

			if ( get_sub_field( 'show_site_notification' ) ) {
				$show_site_notification = get_sub_field('show_site_notification');
			}

			if ( get_sub_field( 'site_notification' ) ) {
				$site_notification = get_sub_field('site_notification');
			}			

    	}
	} 
} else if ( ! class_exists('ACF') ) {
	$dropdown_type = 'hover';
}
?>	

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-945T8FR4ZM"></script>
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-945T8FR4ZM'); </script>
		
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

<?php /* If Site Icon isn't set in customizer */ ?>
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>

	<?php /* Package Generated from: https://realfavicongenerator.net */ ?>
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/apple-touch-icon.png' ); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/favicon-32x32.png' ); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/favicon-16x16.png' ); ?>">
	<link rel="manifest" href="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/site.webmanifest' ); ?>">
	<link rel="mask-icon" href="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/safari-pinned-tab.svg' ); ?>" color="#A92A00">
	<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/favicon.ico' ); ?>">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-config" content="<?php echo esc_url( get_template_directory_uri() . '/includes/images/device-icons/browserconfig.xml' ); ?>">
	<meta name="theme-color" content="#ffffff">

<?php } ?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<?php if ( ! empty( $fb_pixel ) ) { ?>
		<!-- Facebook Pixel -->
		<?php echo $fb_pixel; ?>
		<!-- End Facebook Pixel -->
	<?php } ?>
	
	<?php
		// schema script on Kuan Yin Quantum Healing Session post ONLY 
		if ( is_single(6977) ) { ?>
		<script type="application/ld+json">
			{
			"@context": "https://schema.org",
			"@type": "Product",
			"name": "Kuan Yin Quantum Healing Session",
			"image": [
				"https://centertruehealth.com/wp-content/uploads/2022/09/icon-kuan-yin.svg"
			],
			"description": "Kuan Yin blessings are a quantum field treatment to release negative information at the deepest level, or root cause at the level of the soul.",
			"brand": {
				"@type": "Brand",
				"name": "Center for True Health"
			},
			"offers": {
				"@type": "Offer",
				"price": "350.00",
				"priceCurrency": "USD",
				"availability": "https://schema.org/InStock",
				"seller": {
				"@type": "Organization",
				"name": "Center for True Health"
				}
			}
			}
		</script>

	<?php } ?>

</head>

<body <?php body_class(); ?>>
	<?php if ( ! empty( $show_site_notification ) && $show_site_notification === 'yes' ) { ?>
	<div class="site-notification alert alert-warning alert-dismissible fade show d-none" role="alert">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
	  				<?php echo wp_kses_post( $site_notification ); ?>
	  			</div>
	  		</div>
	  	</div>
		<button type="button" class="close site-note-close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<?php } ?>

	<?php get_template_part( 'sections/book-appointment' ); ?>

	<header class="site-header" role="banner">
		<section class="site-header--booking">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 text-center text-md-right">
						<p class="site-header--booking-links"><i class="far fa-calendar-check"></i> <strong><?php esc_html_e( 'Book Now:', 'tgs_wp' ); ?></strong> <a href="/book-now/"><?php esc_html_e( 'Online', 'tgs_wp' ); ?></a> <a href="mailto:deborah[at]centertruehealth.com?subject=Booking Request"><?php esc_html_e( 'Email', 'tgs_wp' ); ?></a> <a href="tel:+16463261115"><?php esc_html_e( '646.326.1115', 'tgs_wp' ); ?></a></p>
					</div>
				</div>
			</div>
		</section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<nav class="navbar navbar-expand-xl justify-content-between navbar-light <?php echo $dropdown_type; ?>" role="navigation">

						<a class="site-logo navbar-brand d-none d-sm-block" href="/" title="<?php esc_html_e( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-cfth-horizontal.svg' ); ?>" alt="<?php esc_html_e( get_bloginfo( 'name', 'display' ) ); ?>" /></a>

						<a class="site-logo navbar-brand d-block d-sm-none" href="/" title="<?php esc_html_e( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-cfth-horizontal-stacked.svg' ); ?>" alt="<?php esc_html_e( get_bloginfo( 'name', 'display' ) ); ?>" /></a>						

						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" title="Menu">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<span class="menu-text">Menu</span>
						</button>

						<?php 
							wp_nav_menu(
								array(
									'theme_location'  => 'primary',
									'depth'           => 3,
									'container'       => 'div',
									'container_class' => 'collapse navbar-collapse',
									'container_id'    => 'navbarSupportedContent',
									'menu_class'      => 'navbar-nav ml-auto',
									'menu_id'         => 'main-menu',									
									'fallback_cb'     => 'bs4Navwalker::fallback',
									'walker'          => new bs4Navwalker(),
								)
								); 
							?>

					</nav>	
				</div>
			</div>
		</div>	
	</header>