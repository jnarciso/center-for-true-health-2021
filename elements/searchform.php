<?php
/**
 * The template for displaying search forms in tgs_wp
 *
 * @package tgs_wp
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url() ); ?>">
    <div class="input-group">
		<label>Search</label>
		<input type="search" class="form-control search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'tgs_wp' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'tgs_wp' ); ?>">
		<span class="input-group-btn">
			<input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Search', 'submit button', 'tgs_wp' ); ?>">
		</span>
    </div>
</form>
