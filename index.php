<?php
/**
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">

		<?php if ( have_posts() ) { ?>
		<section class="container--blog-post-rows">
			<div class="container">
			<?php while ( have_posts() ) { the_post(); ?>
				<div class="row recording-row d-flex justify-content-lg-center">
					<div class="col-lg-10">

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header>
								<h2><?php the_title(); ?></h2>

								<?php if ( 'post' === get_post_type() ) { ?>
								<div class="entry-meta intro">
									<?php tgs_wp_posted_on(); ?>
								</div>
								<?php } ?>								
							</header>

							<div class="entry-content intro">								
								<?php if ( has_excerpt() ) {
									the_excerpt(); 
								} ?>

								<a href="<?php the_permalink(); ?>" class="button"><?php esc_html_e( 'Read More', 'tgs_wp' ); ?></a>								
							</div>

						</article>

					</div>
				</div>
			<?php } ?>
			</div>

		</section>
		<?php } ?>

		<section class="pagination-links">
			<div class="container"> 
				<div class="row">
					<div class="col-12 text-center">
		    	        <?php tgs_wp_pagination(); ?>
			    	</div>
			    </div>
			</div>
		</section>

	</div>

<?php 
	get_footer();
