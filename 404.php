<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package tgs_wp
 */

get_header(); ?>

	<div class="main-content no-intro" id="main-content" role="main">

		<section class="page-content--container-plain">	
			<div class="container">		
    			<div class="row d-flex justify-content-center">
					<div class="col-md-8 text-center">
							<h2><?php esc_html_e( 'Sorry! That page is not available.', 'tgs_wp' ); ?></h2>
					</div>  
				</div>
				<div class="row d-flex justify-content-center">  				
        			<div class="col-md-8 text-center">
						<p><?php esc_html_e( 'You may not be able to find the page you were looking for because of an out-of-date bookmark, an out-of-date search engine listing or a mis-typed address.', 'tgs_wp'); ?></p>
						<p>Please try one of the links above or visit our <a href="/blog/">blog</a> or <a href="/site-map/">site map</a>.</p>
	        		</div>

        		</div>		    
			</div>
		</section> 

	</div>

<?php 
	get_footer();
