<?php
/**
 * The Template for displaying all single Soul Healing Event.
 *
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content no-intro" id="main-content" role="main">

 	<section class="container--recordings-rows">
    		<div class="container">
	        <?php
	        	$event_intro_blurb = get_field( 'event_intro_blurb' );
	        	$soul_healing_event_form_id = get_field( 'form_selection' );
	        	$gf_output_string = '[gravityform id="' . $soul_healing_event_form_id . '" title="false" description="false" ajax="false"]';
			?>
    			<div class="row recording-row">
					<div class="col-12">
						<!--h3><?php //the_title(); ?></h3-->

		  				<?php if ( !empty( $event_intro_blurb ) ) {
		  					echo wp_kses_post( $event_intro_blurb, 'tgs_wp' ); 
		  				} ?>	

						<div class="recording-row--content">

							<?php if ( have_rows( 'event_content' ) ) {
							 	$i = 0;
							    while ( have_rows( 'event_content' ) ) { the_row(); 
									$event_content_small_title = get_sub_field( 'event_content_small_title' );
									$event_content_large_title = get_sub_field( 'event_content_large_title' );
									$event_content_copy = get_sub_field( 'event_content_copy' );    	
							    ?>
									<?php $i++; ?>

									<?php if ( $i == 3 ): ?>
										<?php break; ?>
									<?php endif; ?>
									<div class="recording-row--event-content">
	  									<h2><?php if ( !empty( $event_content_small_title ) ) { ?><span><?php esc_html_e( $event_content_small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $event_content_large_title ) ) { esc_html_e( $event_content_large_title, 'tgs_wp' ); } ?></h2>
	  									<?php echo wp_kses_post( $event_content_copy ); ?>
									</div>																	
							    <?php }						
							}
							?>	

							<div class="recording-row--event-content">
								<h2><span><?php esc_html_e( 'Purchase This Recording', 'tgs_wp' ); ?></span><br><?php esc_html_e( 'Purchase This Recording For All of the Benefits Above, And More!', 'tgs_wp' ); ?></h2>
								<?php echo do_shortcode( $gf_output_string ); ?>		
							</div>											
						</div>
					</div>						
        		</div>					

        	</div>    

    	</section>

	</div>

<?php 
	get_footer();
