<?php
/**
 * The template for displaying the footer.
 *
 * @package tgs_wp
 */
// check if ACF plugin is installed and active first
if ( class_exists('ACF') ) {
	// get values from our ACF Options/Site Info Page
	if ( have_rows( 'contact_information', 'option' ) ) {
		while ( have_rows( 'contact_information', 'option' ) ) { the_row();		

			if ( get_sub_field( 'primary_phone_number' ) ) {
				$primary_phone = get_sub_field( 'primary_phone_number' );
				$flat_main_phone = '+1' . preg_replace('/[^0-9]/', '', $primary_phone);
			}

			if ( get_sub_field( 'email_address' ) ) {
				$email_address = get_sub_field( 'email_address' );
			}

			if ( get_sub_field( 'address' ) ) {
				$address = get_sub_field( 'address' );
			}

			if ( have_rows( 'social_media_links', 'option' ) ) {
				while ( have_rows( 'social_media_links', 'option' ) ) { the_row();

					if ( get_sub_field( 'facebook_url' ) ) {
						$facebook_url = get_sub_field( 'facebook_url' );
					}

					if ( get_sub_field( 'tiktok_url' ) ) {
						$tiktok_url = get_sub_field( 'tiktok_url' );
					}

					if ( get_sub_field( 'instagram_url' ) ) {
						$instagram_url = get_sub_field( 'instagram_url' );
					}

					if ( get_sub_field( 'pinterest_url' ) ) {
						$pinterest_url = get_sub_field( 'pinterest_url' );
					}

					if ( get_sub_field( 'youtube_url' ) ) {
						$youtube_url = get_sub_field( 'youtube_url' );
					}

					if ( get_sub_field( 'yelp_url' ) ) {
						$yelp_url = get_sub_field( 'yelp_url' );
					}

					if ( get_sub_field( 'linkedin_url' ) ) {
						$linkedin_url = get_sub_field( 'linkedin_url' );
					}																														

				}
			}

    	}
	}
}
?>

	<?php if ( is_page( 5103 ) || is_page( 2695 ) || is_page( 4132 ) ) {
		// check if page is: Practitioners, Biz Starter Kit or 10 things, respectively
		// using page id's incase of page slug changes
		get_template_part('sections/email-signup--practitioner'); 
	} elseif ( !is_page(26) ) { // show email signup on every other page EXCEPT contact/connect
		get_template_part('sections/email-signup');
	} ?>

	<?php if ( !is_page( 'press' ) || !is_page( 22 ) ) {
		get_template_part('sections/oprah-feature'); 
	} ?>	

	<footer class="site-footer" role="contentinfo">
		<section class="site-footer--main">
			<div class="container">
				<div class="row d-flex align-items-center site-footer--address">
					<div class="col-lg-3 text-center text-lg-left">
						<p><strong><?php esc_html_e( get_bloginfo( 'name', 'display' ) ); ?></strong></p>
					</div>
					<div class="col-lg-5 text-center">
					<?php if ( !empty( $address ) ) { ?>
						<address><?php echo wp_kses_post( $address ); ?></address>
					<?php } ?>
					</div>
					<div class="col-lg-4 text-center text-lg-right site-footer--main-social">
						<?php if ( ! empty( $yelp_url ) ) { ?><a href="<?php esc_html_e( $yelp_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on YouTube" target="_blank"><i class="fab fa-yelp"></i></a><?php } ?>							
						<?php if ( ! empty( $facebook_url ) ) { ?><a href="<?php esc_html_e( $facebook_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a><?php } ?>	
						<?php if ( ! empty( $tiktok_url ) ) { ?><a href="<?php esc_html_e( $tiktok_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on TikTok" target="_blank"><i class="fab fa-tiktok"></i></a><?php } ?>	
						<?php if ( ! empty( $youtube_url ) ) { ?><a href="<?php esc_html_e( $youtube_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on YouTube" target="_blank"><i class="fab fa-youtube"></i></a><?php } ?>																		
						<?php if ( ! empty( $pinterest_url ) ) { ?><a href="<?php esc_html_e( $pinterest_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on Pinterest" target="_blank"><i class="fab fa-pinterest-p"></i></a><?php } ?>															
						<?php if ( ! empty( $instagram_url ) ) { ?><a href="<?php esc_html_e( $instagram_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on Instagram" target="_blank"><i class="fab fa-instagram"></i></a><?php } ?>
						<?php if ( ! empty( $linkedin_url ) ) { ?><a href="<?php esc_html_e( $linkedin_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on LinkedIn" target="_blank"><i class="fab fa-linkedin"></i></a><?php } ?>												
					</div>
				</div>
				<div class="row">	
					<div class="col-lg-2 text-center text-lg-left">
						<?php wp_nav_menu(
							array(
								'theme_location' 	=> 'footer-services',
								'depth'             => 2,
								'container'         => '',
								'container_class'   => 'footer-services',
								'container_id'		=> '',
								'menu_class' 		=> 'footer-menu',
							)
						); ?>			
					</div>
					<div class="col-lg-2 text-center text-lg-left">
						<?php wp_nav_menu(
							array(
								'theme_location' 	=> 'footer-gsh',
								'depth'             => 2,
								'container'         => '',
								'container_class'   => 'footer-gsh',
								'container_id'		=> '',
								'menu_class' 		=> 'footer-menu',
							)
						); ?>					
					</div>
					<div class="col-lg-2 text-center text-lg-left">
						<?php wp_nav_menu(
							array(
								'theme_location' 	=> 'footer-practitioners',
								'depth'             => 2,
								'container'         => '',
								'container_class'   => 'footer-practitioners',
								'container_id'		=> '',
								'menu_class' 		=> 'footer-menu',
							)
						); ?>					
					</div>
					<div class="col-lg-2 text-center text-lg-left">
						<?php wp_nav_menu(
							array(
								'theme_location' 	=> 'footer-about',
								'depth'             => 2,
								'container'         => '',
								'container_class'   => 'footer-about',
								'container_id'		=> '',
								'menu_class' 		=> 'footer-menu',
							)
						); ?>					
					</div>	
					<div class="col-lg-2 text-center text-lg-left">
						<?php wp_nav_menu(
							array(
								'theme_location' 	=> 'footer-blog',
								'depth'             => 2,
								'container'         => '',
								'container_class'   => 'footer-blog',
								'container_id'		=> '',
								'menu_class' 		=> 'footer-menu',
							)
						); ?>					
					</div>
					<div class="col-lg-2 text-center text-lg-left">
						<?php wp_nav_menu(
							array(
								'theme_location' 	=> 'footer-book',
								'depth'             => 2,
								'container'         => '',
								'container_class'   => 'footer-book',
								'container_id'		=> '',
								'menu_class' 		=> 'footer-menu',
							)
						); ?>					
					</div>																			
				</div>	
			</div>
		</section>
		<section class="site-footer--sub">
			<div class="container">
				<div class="row d-flex align-items-center">
					<div class="col-lg-6 text-center text-lg-left">
					<?php wp_nav_menu(
						array(
							'theme_location' 	=> 'sub-footer',
							'depth'             => 1,
							'container'         => '',
							'container_class'   => 'sub-footer-menu-container',
							'container_id'		=> '',
							'menu_class' 		=> 'sub-footer-menu list-inline',
						)
					); ?>
					</div>
					<div class="col-lg-6 text-center text-lg-right">
						<p class="copyright">&copy; <?php esc_html_e( current_time('Y') ); ?> <?php esc_html_e( get_bloginfo( 'name', 'display' ) ); ?>. All rights reserved.</p>
					</div>
				</div>
			</div>
		</section>
	</footer>

<?php wp_footer(); ?>
<?php 
	// Script for Popup Domination 
	// https://app.popupdomination.com/
?>
<script src='https://cdn1.pdmntn.com/a/NJzDUSLx-.js'></script>
</body>
</html>
