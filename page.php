<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page--tertiary'); ?>

	<div class="main-content no-intro" id="main-content" role="main">

	<?php get_template_part('sections/page-content-blocks'); ?>

	<?php get_template_part('sections/slider'); ?>

	</div>	

<?php 
	get_footer();
