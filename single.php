<?php
/**
 * The Template for displaying all single posts.
 *
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">

		<?php get_template_part('sections/post-content-blocks'); ?>		
				
		<section class="container--blog-post-rows">
			<div class="container">
			
				<div class="row">
					<div class="col-12">
						<?php // Check Related Posts repeater is populated
							if ( have_rows( 'related_posts' ) ) { ?>
						<h4><?php esc_html_e( 'Related Posts:', 'tgs_wp' ); ?></h4>
						<div class="related-posts">
							<ul class="related-posts-list">								
							<?php while( have_rows( 'related_posts' ) ) { the_row();
        						$related_post = get_sub_field( 'related_post_selector' );
        						$related_post_id = $related_post->ID;
        				        $related_post_excerpt = $related_post->post_excerpt;
        				        $related_post_image = get_post_thumbnail_id( $related_post->ID );
        				        $related_post_image_url = wp_get_attachment_image_url( $related_post_image );        				        
        				    ?>		
        					<li>
        						<div class="row d-flex align-items-center">
        						<?php if ( !empty( $related_post_image ) ) { ?>
        							<div class="col-md-2 text-md-left text-center">
        								<img src="<?php echo esc_url( $related_post_image_url ); ?>" class="img-fluid">
        							</div>
        							<div class="col-md-10 text-md-left text-center">
		        						<h5><a href="<?php echo esc_url( get_permalink( $related_post->ID ) ); ?>"><?php echo esc_html( $related_post->post_title ); ?></a></h5>

		        						<?php if ( !empty( $related_post_excerpt ) ) { ?>
	        							<p><?php echo wp_kses_post( $related_post_excerpt ); ?></p>
		        						<?php } ?> 
        							</div>
        						<?php } else { ?>
        							<div class="col-12 text-md-left text-center">
		        						<h5><a href="<?php echo esc_url( get_permalink( $related_post->ID ) ); ?>"><?php echo esc_html( $related_post->post_title ); ?></a></h5>

		        						<?php if ( !empty( $related_post_excerpt ) ) { ?>
	        							<p><?php echo wp_kses_post( $related_post_excerpt ); ?></p>
		        						<?php } ?>
        							</div>
        						<?php } ?>
        						<?php //print_r($related_post); ?>
        						</div>      						
        					</li>
							<?php unset($related_post_excerpt); } ?>
							</ul>
						</div>							
						<?php } ?>
					</div>
				</div>

			</div>
		</section>		

		<section class="pagination-links">
			<div class="container"> 
				<div class="row">
					<div class="col-md-6 text-center text-md-left">
		    	        <?php previous_post_link( '%link', '&lsaquo; Previous Post', true ); ?>
			    	</div>
					<div class="col-md-6 text-center text-md-right">
		    	        <?php next_post_link( '%link', 'Next Post &rsaquo;', true ); ?>
			    	</div>
			    </div>
			</div>
		</section>

	</div>

<?php 
	get_footer();
