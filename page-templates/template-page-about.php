<?php
/**
 * Template Name: About
 * 
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content no-intro" id="main-content" role="main">

	<?php get_template_part('sections/page-content-blocks'); ?>

	<?php get_template_part('sections/slider'); ?>

	</div>	

<?php 
	get_footer();
