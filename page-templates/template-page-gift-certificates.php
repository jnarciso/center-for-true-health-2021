<?php
/**
 * Template Name: Gift Certificates
 * 
 * @package tgs_wp
 */
get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">

	<?php if ( have_rows( 'page_content_blocks' ) ) { ?>
    	<section class="page-content--container">
    		<div class="container">		
	    <?php while ( have_rows('page_content_blocks') ) { the_row();
	        $page_content_title = get_sub_field( 'page_content_title' );
	        $page_content_copy = get_sub_field( 'page_content_copy' ); ?>

	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $page_content_title ) ) { ?>
	        			<h2><?php esc_html_e( $page_content_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $page_content_copy ) ) { 
	        				echo wp_kses_post( $page_content_copy ); 
	        			} ?>	        			
	        		</div>
	        	</section>
	    <?php } ?>

		<?php 
			$faq_query = new WP_Query( array(
				'post_type'      => 'faq',
				'order'          => 'ASC',
				'orderby'        => 'menu_order',
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
					    'taxonomy' => 'faq_category',
					    'field'    => 'slug',
					    'terms'    => 'gift-certificates',
					),
				),
		));

		if ( $faq_query->have_posts() ) { 
			$i = 1; // Set the increment variable ?>

			<section id="accordion">
	        <?php while ( $faq_query->have_posts() ) {
	        	$faq_query->the_post(); 

				$faq_item_title = get_the_title();
				$faq_item_content = get_field( 'faq_content' );

				if ( $i == 1 ) {
					$faq_container_class = 'collapse show';
					$fa_class = 'far fa-minus';
				} else {
					$faq_container_class = 'collapse';
					$fa_class = 'far fa-plus';
				}		

				?>
					<div class="row d-flex justify-content-lg-center">
						<div class="col-lg-8">
							<div class="faq-item-container">
						    	<header id="heading-<?php echo $i;?>">
						    		<h2 data-toggle="collapse" data-target="#collapse-<?php echo $i;?>" aria-expanded="false" aria-controls="collapse-<?php echo $i;?>">
					        			<i class="far fa-plus"></i> <?php echo $faq_item_title; ?>
						      		</h2>
							    </header>
							
							    <div id="collapse-<?php echo $i;?>" class="collapse" aria-labelledby="heading-<?php echo $i;?>">
							    	<?php // removed from div markup above to eliminate other containers from collapsing: data-parent="#accordion" ?>
						      		<div class="card-body">
							        	<?php echo wp_kses_post( $faq_item_content ); ?>
							      	</div>
							    </div>  
							</div>
					 	</div>
					 </div> 				
					<?php $i++; // Increment the increment variable ?>				

			<?php } // end query while ?>
			</section>
		   
		<?php } // end query if ?>
		<?php wp_reset_postdata(); ?>

	    	</div>
	    </section>
	<?php } ?>

	<?php get_template_part('sections/slider'); ?>

	</div>

<?php get_footer();
