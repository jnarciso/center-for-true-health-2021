<?php
/**
 * Template Name: Soul Healing Recordings
 * 
 * @package tgs_wp
 */
get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">

	<?php if ( have_rows( 'intro_content' ) ) { ?>
		<section class="container--recordings-intro">
			<div class="container">
				<div class="row">
		    	<?php while ( have_rows( 'intro_content' ) ) { the_row(); 
					$small_title = get_sub_field( 'small_title' );
					$large_title = get_sub_field( 'large_title' );
		        	$content = get_sub_field( 'content' );
		        ?>
		        	<div class="col-12 text-center text-lg-left">
		  				<h2><?php if ( !empty( $small_title ) ) { ?><span><?php esc_html_e( $small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $large_title ) ) { esc_html_e( $large_title, 'tgs_wp' ); } ?></h2>

		  				<?php if ( !empty( $content ) ) {
		  					echo wp_kses_post( $content, 'tgs_wp' ); 
		  				} ?>
		        	</div>
	    		<?php } ?>
	    		</div>
    		</div>
    	</section>
    <?php } ?>

    <?php 
		$current_page = get_query_var('paged');
		$current_page = max( 1, $current_page );

		$per_page = 20; // edit to show more if necessary
		$offset_start = 1;
		$offset = ( $current_page - 1 ) * $per_page + $offset_start;
   
    	$soul_healing_event_query = new WP_Query( array(
			'post_type'			=> 'soul_healing_event',
			'posts_per_page' 	=> $per_page,
			'paged'          	=> $current_page,
			'offset'         	=> $offset, // Starts with the second most recent post.
			'orderby'        	=> 'date',  // Makes sure the posts are sorted by date.
			'order'          	=> 'DESC',  // And that the most recent ones come first.
    ));

	// Manually count the number of pages, because we used a custom OFFSET (i.e.
	// other than 0), so we can't simply use $post_list->max_num_pages or even
	// $post_list->found_posts without extra work/calculation.
	$total_rows = max( 0, $soul_healing_event_query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $per_page );

    if ( $soul_healing_event_query->have_posts() ) { ?>
    	<section class="container--recordings-rows">
    		<div class="container">
	        <?php while ( $soul_healing_event_query->have_posts() ) {
	        	$soul_healing_event_query->the_post(); 
	        	$event_intro_blurb = get_field( 'event_intro_blurb' );
			?>
    			<div class="row recording-row">
					<div class="col-12">
						<h3><?php the_title(); ?></h3>

		  				<?php if ( !empty( $event_intro_blurb ) ) {
		  					echo wp_kses_post( $event_intro_blurb, 'tgs_wp' ); 
		  				} ?>	

						<a href="<?php the_permalink(); ?>" class="button"><?php esc_html_e( 'Learn More', 'tgs_wp' ); ?></a>

					</div>						
        		</div>					

    		<?php } // end query while ?>

        	</div>    

        	<?php if ( $total_pages > 1 ) { ?>
			<section class="pagination-links">
				<div class="container"> 
    				<div class="row">
    					<div class="col-12 text-center">
						    <?php echo paginate_links( array(
						        'total'   => $total_pages,
						        'current' => $current_page,
						    ) ); ?>
				    	</div>
				    </div>
				</div>
			</section>
        	<?php } ?>

    	</section>        
    <?php } // end query if ?>
	<?php wp_reset_postdata(); ?>

	<?php get_template_part('sections/slider'); ?>

	</div>

<?php get_footer();
