<?php
/**
 * Template Name: Sessions
 * 
 * @package tgs_wp
 */
get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">
	
	<?php get_template_part('sections/slider'); ?>

	<?php if ( 'sessions_intro_content' ) { ?>
		<section class="sessions-intro-content">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php echo wp_kses_post( get_field( 'sessions_intro_content' ) ); ?>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>

    <?php 
    	$service_query = new WP_Query( array(
			'post_type'      => 'service',
			'post__not_in'   => array(5784), // exclude Past Life Regression
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
    ));

    if ( $service_query->have_posts() ) { $count = ''; ?>
    	<section class="container--service-rows">
    		<div class="container">
	        <?php while ( $service_query->have_posts() ) {
	        	$count++;
	        	$service_query->the_post();
	        	$service_post_id = get_the_id(); 

	        	if ( $service_post_id == 5601 ) {
	        		$cta_link = '/soul-healing-events/';
	        	} else if ( $service_post_id == 5603 ) {
	        		$cta_link = '/soul-healing-recordings/';
	        	} else {
	        		$cta_link = get_the_permalink();
	        	}	        	

	        	if ( $count % 2 ) {
	        		$service_row_left_col_class = 'col-md-6 offset-md-1 order-md-2 service-row--content';
	        		$service_row_right_col_class = 'col-md-5 order-md-1 service-row--image text-center';
	        	} else {
	        		$service_row_left_col_class = 'col-md-6 service-row--content';
	        		$service_row_right_col_class = 'col-md-5 offset-md-1 service-row--image text-center';
	        	}

				if ( have_rows( 'service_intro' ) ) {
					while ( have_rows( 'service_intro' ) ) { the_row(); 
        				$intro_content = get_sub_field( 'intro_content' );						
        				$intro_photo = get_sub_field( 'intro_photo' );
        			?>
    			<div class="row service-row service-row--<?php echo $count; ?>">
					<div class="<?php esc_html_e( $service_row_left_col_class ); ?>">
						<div class="row d-flex align-items-center service-row--title">
							<div class="col-3">
								<?php if ( has_post_thumbnail() ) {
									the_post_thumbnail( '', array('class' => 'img-fluid') );
								} ?>
							</div>
							<div class="col-9 text-left">
								<h2><?php the_title(); ?></h2>
							</div>
						</div>
						<?php if ( !empty( $intro_content ) ) {
							echo wp_kses_post( $intro_content, 'tgs_wp' );
						} ?>
						<a href="<?php echo esc_url( $cta_link ); ?>" class="button"><?php esc_html_e( 'Learn More', 'tgs_wp' ); ?></a>
						<a href="/book-now/" class="button"><?php esc_html_e( 'Book Now', 'tgs_wp' ); ?></a>
					</div>    				
        			<div class="<?php esc_html_e( $service_row_right_col_class ); ?>">
        				<?php if ( !empty( $intro_photo ) ) { ?>
						<img src="<?php echo esc_url( $intro_photo['url'] ); ?>" alt="<?php echo esc_attr( $intro_photo['alt'] ); ?>" class="img-fluid">
        				<? } ?>
	        		</div>
        		</div>
        		    <?php } // end while
        		} // end if ?>

    		<?php } // end query while ?>
        	</div>    
    	</section>        
    <?php } // end query if ?>
	<?php wp_reset_postdata(); ?>

	<?php get_template_part('sections/rates'); ?>

	<?php get_template_part('sections/faq-block'); ?>

	</div>

<?php get_footer();
