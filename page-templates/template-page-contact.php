<?php
/**
 * Template Name: Contact
 * 
 * @package tgs_wp
 */
// check if ACF plugin is installed and active first
if ( class_exists('ACF') ) {
	// get values from our ACF Options/Site Info Page
	if ( have_rows( 'contact_information', 'option' ) ) {
		while ( have_rows( 'contact_information', 'option' ) ) { the_row();		

			if ( get_sub_field( 'primary_phone_number' ) ) {
				$primary_phone = get_sub_field( 'primary_phone_number' );
				$flat_main_phone = '+1' . preg_replace('/[^a-zA-Z0-9]/', '', $primary_phone);
			}

			if ( get_sub_field( 'email_address' ) ) {
				$email_address = get_sub_field( 'email_address' );
			}

			if ( get_sub_field( 'address' ) ) {
				$address = get_sub_field( 'address' );
			}

			if ( get_sub_field( 'google_map_url' ) ) {
				$google_map_url = get_sub_field( 'google_map_url' );
			}

			if ( get_sub_field( 'hours_of_operation' ) ) {
				$hours_of_operation = get_sub_field( 'hours_of_operation' );
			}						

			if ( have_rows( 'social_media_links', 'option' ) ) {
				while ( have_rows( 'social_media_links', 'option' ) ) { the_row();

					if ( get_sub_field( 'facebook_url' ) ) {
						$facebook_url = get_sub_field( 'facebook_url' );
					}

					if ( get_sub_field( 'tiktok_url' ) ) {
						$tiktok_url = get_sub_field( 'tiktok_url' );
					}

					if ( get_sub_field( 'instagram_url' ) ) {
						$instagram_url = get_sub_field( 'instagram_url' );
					}

					if ( get_sub_field( 'pinterest_url' ) ) {
						$pinterest_url = get_sub_field( 'pinterest_url' );
					}

					if ( get_sub_field( 'youtube_url' ) ) {
						$youtube_url = get_sub_field( 'youtube_url' );
					}

					if ( get_sub_field( 'yelp_url' ) ) {
						$yelp_url = get_sub_field( 'yelp_url' );
					}																									

				}
			}

    	}
	}
}

get_header(); ?>

	<?php get_template_part('sections/intro-page--secondary'); ?>

	<div class="main-content" id="main-content" role="main">
	
	<section class="email-signup-contact background-gradient--orange">
			<div class="container">

				<div class="row d-flex justify-content-center">
					<div class="col-lg-4 text-center">
						<h2><span>Connect</span><br>Newsletter Sign Up</h2>
					</div>
				</div>

				<div class="row d-flex justify-content-center intro-content">
					<div class="col-lg-8 text-center">
						<p><?php esc_html_e('Looking for strategies and solutions to help you find true health? Sign up to receive Deborah’s free newsletter. Sent once a month, it includes healing tips, tools, and resources to help you find more balance in your life. (Your information will not be shared with third parties.)', 'tgs_wp'); ?></p>
					</div>
				</div>

				<div class="row">

					<div class="col-lg-3 contact-info">
						<?php if ( !empty ( $flat_main_phone ) ) { ?>
							<p class="contact-info--phone"><a href="tel:<?php esc_html_e( $flat_main_phone ); ?>"><?php esc_html_e( $primary_phone ); ?></a></p>
						<?php } ?>

						<?php if ( !empty ( $email_address ) ) { ?>
							<p class="contact-info--email"><a href="mailto:<?php esc_html_e( $email_address ); ?>"><?php esc_html_e( $email_address ); ?></a></p>
						<?php } ?>

						<div class="contact-info--socials">
							<?php if ( ! empty( $yelp_url ) ) { ?><a href="<?php esc_html_e( $yelp_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on YouTube" target="_blank"><i class="fab fa-yelp"></i></a><?php } ?>							
							
							<?php if ( ! empty( $facebook_url ) ) { ?><a href="<?php esc_html_e( $facebook_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a><?php } ?>	
							
							<?php if ( ! empty( $tiktok_url ) ) { ?><a href="<?php esc_html_e( $tiktok_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on TikTok" target="_blank"><i class="fab fa-tiktok"></i></a><?php } ?>	
							
							<?php if ( ! empty( $youtube_url ) ) { ?><a href="<?php esc_html_e( $youtube_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on YouTube" target="_blank"><i class="fab fa-youtube"></i></a><?php } ?>																		
							<?php if ( ! empty( $pinterest_url ) ) { ?><a href="<?php esc_html_e( $pinterest_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on Pinterest" target="_blank"><i class="fab fa-pinterest-p"></i></a><?php } ?>															
							<?php if ( ! empty( $instagram_url ) ) { ?><a href="<?php esc_html_e( $instagram_url ); ?>" title="<?php esc_html_e( get_bloginfo('name') ); ?> on Instagram" target="_blank"><i class="fab fa-instagram"></i></a><?php } ?>
						</div>						
					</div>		

					<div class="col-lg-7 offset-lg-1">		
						<?php echo do_shortcode( '[gravityform id="3" title="false" description="false" ajax="false"]' ); ?>
					</div>		

				</div>
			</div>
		</section>

		<?php get_template_part('sections/slider'); ?>

	</div>

<?php get_footer();
