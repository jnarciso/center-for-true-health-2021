<?php
/**
 * Template Name: Second Level Page Template
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page--secondary'); ?>

	<div class="main-content" id="main-content" role="main">

	<?php 
		if ( is_page( 'book-now' ) || is_page( 994 ) ) {
			get_template_part('sections/sessions-grid');
		} 
	?>

	<?php get_template_part('sections/page-content-blocks'); ?>

	<?php get_template_part('sections/slider'); ?>

	</div>	

<?php 
	get_footer();
