<?php
/**
 * Template Name: Practitioners
 * 
 * @package tgs_wp
 */
get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">

    	<section class="page-content--container">
    		<div class="container">		

	<?php if ( have_rows( 'book_promo' ) ) { ?>
	    	<section class="practitioner--book-content">
	    		<div class="container">		
		    <?php while ( have_rows('book_promo') ) { the_row();
		    	$book_promo_image = get_sub_field( 'book_promo_image' );
		        $book_promo_small_title = get_sub_field( 'book_promo_small_title' );
		        $book_promo_large_title = get_sub_field( 'book_promo_large_title' );
		        $book_promo_content = get_sub_field( 'book_promo_content' ); 
		    ?>
		        	<div class="row d-flex align-items-center">
		        		<div class="col-lg-5 offset-lg-1 order-lg-2 text-center text-lg-left">
							<h2><?php if ( !empty( $book_promo_small_title ) ) { ?><span><?php esc_html_e( $book_promo_small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $book_promo_large_title ) ) { esc_html_e( $book_promo_large_title, 'tgs_wp' ); } ?></h2>

							<?php if ( !empty( $book_promo_content ) ) {
								echo wp_kses_post( $book_promo_content );
							} ?>

							<p><a href="<?php echo site_url(); ?>/wp-content/uploads/2021/12/Sample-Chapter-for-Building-a-Powerful-Practice-by-Deborah-Flanagan.pdf" target="_blank" class="button"><?php esc_html_e( 'Download Sample Chapter', 'tgs_wp' ); ?></a></p>

							<p><a href="/practitioners/business-starter-kit/" class="button"><?php esc_html_e( 'Get the Business Starter Kit', 'tgs_wp' ); ?></a></p>

							<p><?php esc_html_e( 'Learn all about 10 Things Nobody Tells You About Starting Your Own Business.', 'tgs_wp' ); ?></p>
							<p><a href="/practitioners/10-things-nobody-tells-you-about-starting-your-own-business/" class="button"><?php esc_html_e( 'Read Now', 'tgs_wp' ); ?></a></p>
		        		</div>
		        		<?php if ( !empty( $book_promo_image ) ) { ?>
		        		<div class="col-lg-6 order-lg-1 text-center image-column">
		        			<img src="<?php echo esc_url( $book_promo_image['url'] ); ?>" alt="<?php echo esc_attr( $book_promo_image['alt']  ); ?>" class="img-fluid">
		        		</div>
		        		<?php } ?>	        		
		        	</div>
		    <?php } ?>
		    	</div>
		    </section>
	<?php } ?>

	<?php if ( have_rows( 'practitioner_page_content_blocks' ) ) { ?>
	    <?php while ( have_rows('practitioner_page_content_blocks') ) { the_row();
	        $page_content_title = get_sub_field( 'page_content_title' );
			$hash_link = str_replace(' ', '-', $page_content_title); // Replaces all spaces with hyphens
			$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
			$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
			$hash_link = strtolower( $hash_link ); // Lowercase the string
	        $page_content_copy = get_sub_field( 'page_content_copy' ); ?>

	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $page_content_title ) ) { ?>
	        			<h2 id="<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $page_content_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $page_content_copy ) ) { 
	        				echo wp_kses_post( $page_content_copy ); 
	        			} ?>	        			
	        		</div>
	        	</section>
	    <?php } ?>

	    <?php if ( is_page('business-starter-kit') || is_page(2695) ) { ?>
	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<h2><?php esc_html_e( 'Subscribe to the Practitioner Newsletter', 'tgs_wp' ); ?></h2>
	        			<?php echo do_shortcode( '[gravityform id="5" title="false" description="false" ajax="false"]' ); ?>
	        		</div>
	        	</section>
	    <?php } ?>

	<?php } ?>
	    	</div>
	    </section>

	<?php get_template_part('sections/slider'); ?>

	</div>	

<?php 
	get_footer();
