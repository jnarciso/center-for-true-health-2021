<?php
/**
 * Template Name: Site Map
 * 
 * @package tgs_wp
 */

get_header(); ?>

	<?php get_template_part('sections/intro-page--tertiary'); ?>

	<div class="main-content no-intro" id="main-content" role="main">

    	<section class="page-content--container">
    		<div class="container">		
	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<h2><?php esc_html_e( 'Pages', 'tgs_wp' ); ?></h2>
						<ul>
                  			<?php 
                  				// exclude For Clients and children of For Clients pages
                  				$descendants = get_pages( array( 'child_of' => 44 ) );
                  				$exclude = '';
                  				foreach ( $descendants as $page ) {
									$exclude .= $page->ID . ",";									
								}
								// exclude Thank You page for Recording purchases
								$exclude .= 4916;
                  				wp_list_pages( array(
              					    'title_li'	=> '',
                  					'exclude'	=> $exclude,
                  				) ); 
                  			?>
              			</ul>
	        		</div>
	        	</section>

	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<h4><?php esc_html_e( 'Services', 'tgs_wp' ); ?></h4>
                        <?php 
                        	$service_query = new WP_Query( array(
								'post_type'      => 'service',
								'order'          => 'ASC',
								'orderby'        => 'menu_order',
								'post_status'    => 'publish',
								'posts_per_page' => -1
                        ));
                        // The Loop
                        if ( $service_query->have_posts() ) { ?>
                    	<ul>
                            <?php while ( $service_query->have_posts() ) {
                            	$service_query->the_post(); 
                            ?>
							<li><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></li>
                            <?php } ?>    
                        </ul>
                        <?php } ?>

						<?php wp_reset_postdata(); ?>	        			
	        		</div>
	        	</section>

	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<h4><?php esc_html_e( 'Blog Posts', 'tgs_wp' ); ?></h4>
						<ul>
                  			<?php wp_get_archives('type=alpha'); ?>
              			</ul>
	        		</div>
	        	</section>	        	
	    	</div>
	    </section>

	<?php get_template_part('sections/slider'); ?>

	</div>	

<?php 
	get_footer();
