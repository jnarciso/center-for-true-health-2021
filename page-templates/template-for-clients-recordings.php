<?php
/**
 * Template Name: For Clients Recordings
 * 
 * @package tgs_wp
 */
get_header(); ?>

	<?php get_template_part('sections/intro-page'); ?>

	<div class="main-content" id="main-content" role="main">

	<?php if ( have_rows( 'intro_content' ) ) { ?>
		<section class="container--recordings-intro">
			<div class="container">
				<div class="row">
		    	<?php while ( have_rows( 'intro_content' ) ) { the_row(); 
					$small_title = get_sub_field( 'small_title' );
					$large_title = get_sub_field( 'large_title' );
		        	$content = get_sub_field( 'content' );
		        ?>
		        	<div class="col-12 text-center text-lg-left">
		  				<h2><?php if ( !empty( $small_title ) ) { ?><span><?php esc_html_e( $small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $large_title ) ) { esc_html_e( $large_title, 'tgs_wp' ); } ?></h2>

		  				<?php if ( !empty( $content ) ) {
		  					echo wp_kses_post( $content, 'tgs_wp' ); 
		  				} ?>
		        	</div>
	    		<?php } ?>
	    		</div>
    		</div>
    	</section>
    <?php } ?>

	<?php get_template_part('sections/page-content-blocks'); ?>

	</div>

<?php get_footer();
