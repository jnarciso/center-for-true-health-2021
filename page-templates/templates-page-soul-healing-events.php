<?php
/**
 * Template Name: Soul Healing Events
 * 
 * @package tgs_wp
 */

get_header(); ?>

	<div class="main-content no-intro" id="main-content" role="main">

	<?php get_template_part( 'sections/book-appointment' ); ?>

    <?php 
    	// start query for latest Soul Healing Event
    	$soul_healing_event_query = new WP_Query( array(
			'post_type'      	=> 'soul_healing_event',
			'orderby'        	=> 'date',  // Makes sure the posts are sorted by date.
			'order'          	=> 'DESC',  // And that the most recent ones come first.
			'post_status'    	=> 'publish',
			'posts_per_page' 	=> 1,
    ));

    if ( $soul_healing_event_query->have_posts() ) { ?>
		<section class="soul-healing-event--intro">
			<div class="container">
        <?php while ( $soul_healing_event_query->have_posts() ) {
        	$soul_healing_event_query->the_post();
        	$event_image = get_field( 'event_image' );
        	$event_sign_up_link = get_field( 'event_sign_up_link' );
        ?>

				<?php if ( !empty( $event_image ) ) { ?>
				<div class="row d-flex justify-content-center">
					<div class="col-lg-8 text-center soul-healing-event--image-col">
						<img src="<?php echo esc_url( $event_image['url'] ); ?>" alt="<?php echo esc_attr( $event_image['alt'] ); ?>">
					</div>
				</div>
				<?php } ?>

				<?php
					// setup tabs for event info
					if ( have_rows( 'event_content' ) ) {

					?>

						<?php 
							while ( have_rows( 'event_content' ) ) { the_row();
								$event_content_link_text = get_sub_field( 'event_content_link_text' );
								$event_content_small_title = get_sub_field( 'event_content_small_title' );
								$event_content_large_title = get_sub_field( 'event_content_large_title' );
								$event_content_copy = get_sub_field( 'event_content_copy' );
							?>
				<div class="row d-flex justify-content-center soul-healing-event--content-col">
					<div class="col-12">

	  							<?php if ( !empty( $event_content_small_title ) || !empty( $event_content_large_title ) ) { ?>
	  							<div class="row d-flex justify-content-center">
	  								<div class="col-lg-6 text-center">
	  									<h2><?php if ( !empty( $event_content_small_title ) ) { ?><span><?php esc_html_e( $event_content_small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $event_content_large_title ) ) { esc_html_e( $event_content_large_title, 'tgs_wp' ); } ?></h2>
	  								</div>
	  							</div>
	  							<?php } ?>

	  							<?php if ( !empty( $event_content_copy ) ) { ?>
	  							<div class="row d-flex justify-content-center">
	  								<div class="col-10">
	  									<?php echo wp_kses_post( $event_content_copy ); ?>	
	  								</div>
	  							</div>
	  							<?php } ?>
	  							
					</div>
				</div>	
						<?php } ?>

				<?php } ?>

				<div class="row">
					<div class="col-12 text-center">
						<?php if ( !empty( $event_sign_up_link ) ) { ?><a href="<?php echo esc_url( $event_sign_up_link ); ?>" target="_blank" class="button">Sign Up For This Event</a><?php } ?> <a href="/soul-healing-recordings/" class="button">View Soul Healing Recordings</a>
					</div>
				</div>


    	<?php } ?> 
        	</div>   
		</section>
	<?php } ?>

		<?php wp_reset_postdata(); ?>

		<?php get_template_part('sections/slider'); ?>		

		<?php get_template_part('sections/faq-block'); ?>

	</div>

<?php 
	get_footer();
