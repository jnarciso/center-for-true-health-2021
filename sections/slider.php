<?php
/**
 * The template for default slick slider
 *
 * @package tgs_wp
 */
?>

<?php 
    // get slug of page we're on (single, etc)
    // use it below to map testimonial type by same taxonomy name
    global $post;
    $post_slug = $post->post_name;

    // start the cpt/taxonomy query
    $testimonial_query = new WP_Query( array(
        'post_type'      => 'testimonial',
        'order'          => 'ASC',
        'orderby'        => 'menu_order',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'testimonial_category',
                'field'    => 'slug',
                'terms'    => $post_slug,
            ),
        ),
    ));

    if ( $post->ID == 26 ) {
        $bg_color_style = 'background-gradient--green';
    } else {
        $bg_color_style = 'background-gradient--orange';
    }
    
    // if the posts are available, populate structure
    if ( $testimonial_query->have_posts() ) { ?>
        <section class="testimonial-slider--container">
        
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="testimonial-slider slick-slideshow <?php echo $bg_color_style; ?>">

                            <?php while ( $testimonial_query->have_posts() ) {
                                $testimonial_query->the_post();
                                $testimonial_large_text = get_field( 'testimonial_large_text' );
                                $testimonial_small_text = get_field( 'testimonial_small_text' ); 
                            ?>
                            <div class="slide-item">
                                <div class="container">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-10">
                                            <div class="slide-content">
                                                <?php if ( !empty( $testimonial_large_text ) ) { ?>
                                                    <div class="testimonial-large-text">
                                                        <?php echo wp_kses_post( $testimonial_large_text, 'tgs_wp' ); ?>
                                                    </div>
                                                <? } ?>
                                                <?php if ( !empty( $testimonial_small_text ) ) { ?>
                                                    <div class="testimonial-small-text">
                                                        <?php echo wp_kses_post( $testimonial_small_text, 'tgs_wp' ); ?>
                                                        <p class="testimonial-name"><em><?php the_title(); ?></em></p>
                                                    </div>
                                                <? } ?>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>    

                        </div>
                    </div>                
                </div>                        
            </div>
        </section>

    <?php } ?>

<?php wp_reset_postdata(); ?>

        



