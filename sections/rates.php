	<section class="rates-cta-section background-gradient--orange">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 offset-lg-5 col-xl-6 order-lg-2 text-center text-lg-left rates-cta--content">
					<div class="rates-cta--content-inner">
						<h5><?php esc_html_e( 'Ready to find your true health balance?', 'tgs_wp' ); ?></h5>
						<p>Session rates vary according to duration. See the full list of options on the Rates page. <strong>Gift certificates are also available.</strong></p>
						<a href="/sessions/rates/" class="button">View Rates</a> <a href="/sessions/gift-certificates/" class="button">Gift Certificates</a>
					</div>
				</div>				
				<div class="col-lg-6 order-lg-1 text-center text-lg-right rates-cta--image">
					<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/rates-block--image-new.jpg' ); ?>" alt="Woman at computer" class="img-fluid">
				</div>
			</div>
		</div>
	</section>