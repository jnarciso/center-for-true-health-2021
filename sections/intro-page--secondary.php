	<?php 

	if ( have_rows( 'secondary_page_intro' ) ) { ?>
	<section class="page-intro--page-secondary">
		<div class="container">		
			<div class="row d-flex align-items-center">
	    <?php while ( have_rows( 'secondary_page_intro') ) { the_row(); 
	    	$secondary_small_title = get_sub_field( 'secondary_small_title' );
	    	$secondary_large_title = get_sub_field( 'secondary_large_title' );
	    	$secondary_intro_content = get_sub_field( 'secondary_intro_content' );	    	
	        $secondary_page_intro_image = get_sub_field( 'secondary_page_intro_image' );
        ?>
        		<?php if ( !empty( $secondary_small_title ) || !empty( $secondary_large_title ) || !empty( $secondary_intro_content ) ) { ?>
				<div class="col-lg-5 offset-lg-1 text-center text-lg-left page-intro--page-content">
					<h1><?php if ( !empty( $secondary_small_title ) ) {?><span><?php esc_html_e( $secondary_small_title, 'tgs_wp' ); ?></span><br><?php } ?> <?php if ( !empty( $secondary_large_title ) ) { esc_html_e( $secondary_large_title, 'tgs_wp' ); } ?></h1>

					<?php if ( !empty( $secondary_intro_content ) ) {
						echo wp_kses_post( $secondary_intro_content );
					} ?>
				</div>
				<?php } ?>				

				<?php if ( !empty( $secondary_page_intro_image ) ) { ?>
				<div class="col-lg-6 text-center text-lg-left page-intro--page-image">
					<img src="<?php echo esc_url( $secondary_page_intro_image['url'] ); ?>" alt="<?php echo esc_attr( $secondary_page_intro_image['alt'] ); ?>" class="img-fluid">
				</div>
				<?php } ?>
			        
	    <?php } ?>
	    	</div>
		</div>

    </section>
	<?php } ?>

