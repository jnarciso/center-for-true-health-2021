	<?php if ( have_rows( 'business_starter_kit' ) ) { ?>
    	<section class="practitioner--business-starter-kit">
    		<div class="container">		
	    <?php while ( have_rows('business_starter_kit') ) { the_row();
	        $bsk_small_title = get_sub_field( 'business_starter_kit_small_title' );
	        $bsk_large_title = get_sub_field( 'business_starter_kit_large_title' );
	        $bsk_content = get_sub_field( 'business_starter_kit_content' ); 
	    ?>
	        	<div class="row d-flex align-items-center">
	        		<div class="col-12 text-center">
						<h2><?php if ( !empty( $bsk_small_title ) ) { ?><span><?php esc_html_e( $bsk_small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $bsk_large_title ) ) { esc_html_e( $bsk_large_title, 'tgs_wp' ); } ?></h2>

						<?php if ( !empty( $bsk_content ) ) {
							echo wp_kses_post( $bsk_content );
						} ?>

						<a href="/practitioners/business-starter-kit/" class="button white"><?php esc_html_e( 'Learn More', 'tgs_wp' ); ?></a>
	        		</div>       		
	        	</div>
	    <?php } ?>
	    	</div>
	    </section>
	<?php } ?>