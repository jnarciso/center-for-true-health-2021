	<?php if ( have_rows( 'intro_section' ) ) { ?>
		<section class="page-intro--home">
			<div class="container">
				<div class="row d-flex align-items-center">

		    	<?php while ( have_rows( 'intro_section' ) ) { the_row(); 
					$intro_title = get_sub_field( 'intro_title' );
					$intro_content = get_sub_field( 'intro_content' );
		        	$intro_image = get_sub_field( 'intro_image' );
		        ?>

		        <?php if ( !empty( $intro_title ) || !empty( $intro_content ) ) { ?>
		        	<div class="col-lg-6 offset-lg-1 content-column">
		        		<div class="page-intro--home-content text-center text-lg-left">
							
							<span class="welcome-text d-none d-lg-block">Welcome</span>

			        		<?php if ( !empty( $intro_title ) ) { ?>
			        		<h1><?php esc_html_e( $intro_title, 'tgs_wp' ); ?></h1>
			        		<?php } ?>

			        		<?php if ( !empty( $intro_content ) ) {
			        			echo wp_kses_post( $intro_content, 'tgs_wp' );
			        		} ?>

			        		<?php 
			        			// check for buttons
								if ( have_rows( 'intro_buttons' ) ) {
	    							while ( have_rows( 'intro_buttons' ) ) { the_row();
	        						$button_text = get_sub_field( 'button_text' );
	        						$button_type = get_sub_field( 'button_type' );
	        						$button_link = ''; 

	        						if ( $button_type == 'internal' ) {
	        							$button_link = get_sub_field( 'button_link_page' );
	        						} else {
	        							$button_link = get_sub_field( 'button_link_url' );
	        						}

	        					?>
	    					<a href="<?php echo esc_url( $button_link ); ?>" class="button"><?php esc_html_e( $button_text, 'tgs_wp' ); ?></a>
								<?php }
							} ?>
						</div>
		        	</div>
		        <?php } ?>

		        <?php if ( !empty( $intro_image ) ) { ?>
		        	<div class="col-lg-6 hero-image text-center">
						<img src="<?php echo esc_url( $intro_image['url'] ); ?>" alt="<?php echo esc_attr( $intro_image['alt'] ); ?>" class="img-fluid">
					</div>
		        <?php } ?>

    			<?php } ?>
    			</div>    			

    		</div>

			<!--a href="#main-content" class="link-skip-arrow"><i class="fas fa-chevron-down"></i></a-->
		</section>    	
	<?php } ?>