    <?php 
    	$service_query = new WP_Query( array(
			'post_type'      => 'service',
			'post__not_in'	 =>  array(5784),
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
    ));

    if ( $service_query->have_posts() ) { ?>
    	<section class="service-grid--home">
    		<div class="container">

			<?php if ( have_rows( 'available_sessions_section' ) ) { ?>
				<div class="row d-flex align-items-center service-grid--home-intro">
		    	<?php while ( have_rows( 'available_sessions_section' ) ) { the_row(); 
					$small_title = get_sub_field( 'small_title' );
					$large_title = get_sub_field( 'large_title' );
		        	$intro_content = get_sub_field( 'intro_content' );
		        ?>
		        	<div class="col-lg-4 text-center text-lg-left">
		        		<h2><span><?php esc_html_e( $small_title, 'tgs_wp' ); ?></span><br><?php esc_html_e( $large_title, 'tgs_wp' ); ?></h2>
		        	</div>
		        	<div class="col-lg-6 offset-lg-2 text-center text-lg-left">
		        		<?php echo wp_kses_post( $intro_content, 'tgs_wp' ); ?>
		        	</div>
		    	<?php } ?>
		    	</div>
		    <?php } ?>

    			<div class="row d-flex justify-content-center">
	        <?php while ( $service_query->have_posts() ) {
	        	$service_query->the_post(); 
	        	$service_excerpt = get_field( 'service_excerpt' );
	        	$service_post_id = get_the_id();
	        	$cta_link = '';
	        	$cta_text = 'Learn More';

	        	if ( $service_post_id == 5601 ) {
	        		$cta_link = '/soul-healing-events/';
	        	} else if ( $service_post_id == 5603 ) {
	        		$cta_link = '/soul-healing-recordings/';
	        	} else {
	        		$cta_link = get_the_permalink();
	        	}

	        	if ( is_page( 'book-now' ) || is_page( 994 ) ) {
	        		$cta_link = '';
	        		$cta_text = 'Book Now';
	        		$cta_link = get_field( 'direct_booking_link' );
	        		
	        		if ( empty( $cta_link ) ) {
	        			$cta_link = get_the_permalink();
	        			$cta_text = 'Learn More';
	        		}
	        		

					if ( $service_post_id == 5601 ) {
						$cta_link = '/soul-healing-events/';
					}

	        	}

	        ?>
					<div class="col-md-5 text-center">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail();
						} ?>
						<h2><?php the_title(); ?></h2>
						<?php if ( !empty( $service_excerpt ) ) {
							echo wp_kses_post( $service_excerpt, 'tgs_wp' );
						} ?>
						<a href="<?php echo esc_url( $cta_link ); ?>"><span><?php esc_html_e( $cta_text, 'tgs_wp' ); ?></span></a>
					</div>
    		<?php } ?>
        		</div>

        	</div>    
    	</section>        
    <?php } ?>
	<?php wp_reset_postdata(); ?>