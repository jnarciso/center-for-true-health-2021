	
    	<section class="page-content--container">
    		<div class="container">	

    	<?php 
    		// check if we want to add jump links to this page
    		// build our links to jump to hashed sections below
    		if ( get_field( 'add_jump_links' ) == 'yes' ) { 

    			// check for jump link intro content
    			if ( have_rows( 'jump_links_intro_content' ) ) {
    				while ( have_rows('jump_links_intro_content' ) ) { the_row(); 
    				$jump_links_intro_title = get_sub_field( 'jump_links_intro_title' );
    				$jump_links_intro_content = get_sub_field( 'jump_links_intro_content' );	
    			?>
	        	<section class="row d-flex justify-content-center jump-link-intro-content">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $jump_links_intro_title ) ) { ?>
	        			<h2><?php esc_html_e( $jump_links_intro_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $jump_links_intro_content ) ) { 
	        				echo wp_kses_post( $jump_links_intro_content ); 
	        			} ?>	        			
	        		</div>
	        	</section>
    				<?php } 
    			} ?>

    		<?php if ( have_rows( 'page_content_blocks' ) ) { ?>
    			<section class="row d-flex justify-content-center jump-link-section">
    				<div class="col-lg-8">
    					<ul class="plain-list">
				    <?php while ( have_rows('page_content_blocks') ) { the_row(); 
				    	$page_content_title = get_sub_field( 'page_content_title' );
   						$hash_link = str_replace(' ', '-', $page_content_title); // Replaces all spaces with hyphens
   						$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
						$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
						$hash_link = strtolower( $hash_link ); // Lowercase the string
				   	?>
				    		<li><a href="#<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $page_content_title ); ?></a></li>
			    	<?php } ?>
			    		</ul>
    				</div>
    			</section>
			<?php } ?>    				
    	<? } ?>

    	<?php reset_rows(); ?>

	<?php if ( have_rows( 'page_content_blocks' ) ) { ?>
	    <?php while ( have_rows('page_content_blocks') ) { the_row();
	        $page_content_title = get_sub_field( 'page_content_title' );
			$hash_link = str_replace(' ', '-', $page_content_title); // Replaces all spaces with hyphens
			$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
			$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
			$hash_link = strtolower( $hash_link ); // Lowercase the string
	        $page_content_copy = get_sub_field( 'page_content_copy' ); ?>

	        	<section class="row d-flex justify-content-center">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $page_content_title ) ) { ?>
	        			<h2 id="<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $page_content_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $page_content_copy ) ) { 
	        				echo wp_kses_post( $page_content_copy ); 
	        			} ?>	        			
	        		</div>
	        	</section>
	    <?php } ?>

	<?php } ?>
	    	</div>
	    </section>	