	
	<section class="page-intro--page-plain tertiary">
		<div class="container">		
			<div class="row d-flex align-items-center">
				<div class="col-12 text-center page-intro--page-content">
					<h1><?php the_title(); ?></h1>
				</div>
	    	</div>
		</div>

    </section>
