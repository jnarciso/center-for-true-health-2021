	
    	<section class="page-content--container">
    		<div class="container">	

    	<?php 
    		// check if we want to add jump links to this page
    		// build our links to jump to hashed sections below
    		if ( get_field( 'add_post_jump_links' ) == 'yes' ) { 

    			// check for jump link intro content
    			if ( have_rows( 'post_jump_links_intro_content' ) ) {
    				while ( have_rows('post_jump_links_intro_content' ) ) { the_row(); 
    				$jump_links_intro_title = get_sub_field( 'post_jump_links_intro_title' );
    				$jump_links_intro_content = get_sub_field( 'post_jump_links_intro_content' );	
    			?>
	        	<section class="row d-flex justify-content-center jump-link-intro-content">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $jump_links_intro_title ) ) { ?>
	        			<h2><?php esc_html_e( $jump_links_intro_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $jump_links_intro_content ) ) { 
	        				echo wp_kses_post( $jump_links_intro_content ); 
	        			} ?>	        			
	        		</div>
	        	</section>
    				<?php } 
    			} ?>

    			<?php if ( have_rows( 'post_content_blocks' ) ) { ?>
    			<section class="row d-flex justify-content-center jump-link-section">
    				<div class="col-lg-8">
    					<ul class="plain-list">
				    <?php while ( have_rows('post_content_blocks') ) { the_row(); 
				    	$post_content_title = get_sub_field( 'post_content_title' );
   						$hash_link = str_replace(' ', '-', $post_content_title); // Replaces all spaces with hyphens
   						$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
						$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
						$hash_link = strtolower( $hash_link ); // Lowercase the string
				   	?>
				    		<li><a href="#<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $post_content_title ); ?></a></li>
			    	<?php } ?>
			    		</ul>
    				</div>
    			</section>
			<?php } ?>    				
    	<? } else { ?>

				<div class="row">
					<div class="col-12">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">									
								<?php the_content(); ?>
							</div>
						</article>
					</div>
				</div>

    	<?php } ?>

    	<?php reset_rows(); ?>

	<?php if ( have_rows( 'post_content_blocks' ) ) { ?>
	    <?php while ( have_rows('post_content_blocks') ) { the_row();
	        $post_content_title = get_sub_field( 'post_content_title' );
			$hash_link = str_replace(' ', '-', $post_content_title); // Replaces all spaces with hyphens
			$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
			$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
			$hash_link = strtolower( $hash_link ); // Lowercase the string
	        $post_content_copy = get_sub_field( 'post_content_copy' ); ?>

	        	<section class="row d-flex justify-content-center no-bottom-margin">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $post_content_title ) ) { ?>
	        			<h2 id="<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $post_content_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $post_content_copy ) ) { ?>
						<article class="post">
							<div class="entry-content">									
								<?php echo wp_kses_post( $post_content_copy ); ?>
							</div>
						</article>	        				
	        			<?php } ?>	        			
	        		</div>
	        	</section>
	    <?php } ?>

	<?php } ?>
	    	</div>
	    </section>	