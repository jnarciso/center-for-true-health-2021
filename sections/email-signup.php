	<section class="email-signup">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10 text-center email-signup--outer-column">
					<div class="email-signup--content">		
						<div class="row d-flex justify-content-center">		
							<div class="col-lg-8 text-center">
								<header>
									<h4><span><?php esc_html_e( 'Get Started Today', 'tgs_wp' ); ?></span><br><?php esc_html_e( 'Now is the time to find your center', 'tgs_wp' ); ?></h4>
								</header>
								<p><?php esc_html_e( 'Each month Deborah offers curated soul healing resources and shares the top tools, tips, and practices she personally finds helpful.', 'tgs_wp' ); ?></p>
								<?php echo do_shortcode( '[gravityform id="2" title="false" description="false" ajax="false"]' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>