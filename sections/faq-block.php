	<section class="faq-block">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10 text-center faq-block--outer-column">
					<div class="faq-block--content">
						<div class="row d-flex justify-content-center">
							<div class="col-lg-7">
								<header>
									<h2><span><?php esc_html_e( 'Frequently Asked Questions', 'tgs_wp' ); ?></span><br><?php esc_html_e( 'Questions? Find the answers you\'re looking for.', 'tgs_wp' ); ?></h2>
								</header>
							</div>
						</div>
						<div class="row d-flex justify-content-center">
							<div class="col-md-9">
								<?php if ( is_page( 'soul-healing-events' ) || is_page( 5101 ) ) { ?>
								<p><?php esc_html_e( 'Browse FAQ\'s regarding Soul Healing Events, results and rates.', 'tgs_wp' ); ?></p>
								<a href="#soul-healing-event--faqs" class="button"><?php esc_html_e( 'Read FAQs', 'tgs_wp' ); ?></a>								
								<?php } else { ?>
								<p><?php esc_html_e( 'Browse FAQ\'s regarding virtual sessions, potential benefits, and rates.', 'tgs_wp' ); ?></p>
								<a href="/sessions/faqs/" class="button"><?php esc_html_e( 'Read FAQs', 'tgs_wp' ); ?></a>								
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php if ( is_page( 'soul-healing-events' ) || is_page( 5101 ) ) { // check if we're on Soul Healing Page 

    	$faq_query = new WP_Query( array(
			'post_type'      => 'faq',
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
		    'tax_query' => array(
				array(
				    'taxonomy' => 'faq_category',
				    'field'    => 'slug',
				    'terms'    => 'soul-healing-events',
				),
    		),
    ));

    if ( $faq_query->have_posts() ) { 
    	$i = 1; // Set the increment variable ?>
    	<section class="soul-healing-event--faqs page-content--container" id="soul-healing-event--faqs">
    		<div class="container">

    			<section id="accordion">    	
	        <?php while ( $faq_query->have_posts() ) {
	        	$faq_query->the_post(); 

				$faq_item_title = get_the_title();
				$faq_item_content = get_field( 'faq_content' );

				if ( $i == 1 ) {
					$faq_container_class = 'collapse show';
					$fa_class = 'far fa-minus';
				} else {
					$faq_container_class = 'collapse';
					$fa_class = 'far fa-plus';
				}		

				?>
					<div class="row d-flex justify-content-lg-center">
						<div class="col-12">
							<div class="faq-item-container">
						    	<header id="heading-<?php echo $i;?>">
						    		<h5 data-toggle="collapse" data-target="#collapse-<?php echo $i;?>" aria-expanded="false" aria-controls="collapse-<?php echo $i;?>">
					        			<i class="far fa-plus"></i> <?php echo $faq_item_title; ?>
						      		</h5>
							    </header>
							
							    <div id="collapse-<?php echo $i;?>" class="collapse" aria-labelledby="heading-<?php echo $i;?>">
							    	<?php // removed from div markup above to eliminate other containers from collapsing: data-parent="#accordion" ?>
						      		<div class="card-body">
							        	<?php echo wp_kses_post( $faq_item_content ); ?>
							      	</div>
							    </div>  
							</div>
					 	</div>
					 </div> 				
					<?php $i++; // Increment the increment variable ?>				

    		<?php } // end query while ?>  

    			</section>

        	</div>    
    	</section> 

    <?php } ?>

	<?php wp_reset_postdata(); ?>



	<?php } ?>