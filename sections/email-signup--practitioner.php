	<section class="email-signup practitioner">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-9 col-lg-6 text-center">
					<header>
						<h4><span><?php esc_html_e( 'Build A Powerful Practice', 'tgs_wp' ); ?></span><br><?php esc_html_e( 'Subscribe to the Practitioner Newsletter', 'tgs_wp' ); ?></h4>
					</header>
					<p><?php esc_html_e( 'Deborah shares insights gleaned from her own experiences, plus advice from other accomplished practitioners.', 'tgs_wp' ); ?></p>
					<?php echo do_shortcode( '[gravityform id="11" title="false" description="false" ajax="false"]' ); ?>
				</div>
			</div>
		</div>
	</section>