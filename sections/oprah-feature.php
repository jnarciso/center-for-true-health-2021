	<section class="oprah-feature">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10 text-center oprah-feature--outer-column">
					<div class="oprah-feature--content">
						<div class="row d-flex justify-content-center">
							<div class="col-md-8">
								<header>
									<h4><?php esc_html_e( 'Featured in Oprah Magazine', 'tgs_wp' ); ?></h4>
								</header>
							</div>
						</div>
						<div class="row d-flex justify-content-center">
							<div class="col-md-9">
								<p><?php esc_html_e( 'Burned out by an all-consuming job, Deborah Flanagan found balance and peace through the healing arts — and now she\'s helping others do the same.', 'tgs_wp' ); ?></p>

								<?php if ( is_front_page() ) { ?>
								<a href="/about/" class="button"><?php esc_html_e( 'Read Deborah\'s Story', 'tgs_wp' ); ?></a>
								<?php } else { ?>
								<a href="/about/press/" class="button"><?php esc_html_e( 'Read The Press', 'tgs_wp' ); ?></a>
								<?php } ?>
								
								<h6><?php esc_html_e( 'Also Featured On', 'tgs_wp' ); ?></h6>
							</div>
						</div>
						<div class="row d-flex align-items-center">
							<div class="col-12 text-center press-logo-container">								
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-mind-body-green.svg' ); ?>" alt="Mind Body Green Logo">
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-oprah-magazine.svg' ); ?>" alt="Oprah Magazine Logo">
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-dr-oz.svg' ); ?>" alt="Dr. Oz Logo"><br>
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-your-tango.svg' ); ?>" alt="Your Tango Logo">
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-abc.svg' ); ?>" alt="ABC Network Logo">
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-black-enterprise-magazine.svg' ); ?>" alt="Black Enterprise Magazine Logo">								
								<img src="<?php echo esc_url( get_template_directory_uri() . '/includes/images/logo-featured-z-living.svg' ); ?>" alt="Z Living Logo">					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>