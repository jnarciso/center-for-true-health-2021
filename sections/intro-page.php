	<?php 
	if ( is_singular( 'soul_healing_event' ) || is_page_template( 'page-templates/template-for-clients-recordings.php' ) ) {
		$field_page_id = 5358; // Soul Healing Recording page id
	} elseif ( is_home() || is_single() ) {
		$field_page_id = get_option( 'page_for_posts' );
	} else {
		$field_page_id = get_the_ID();
	}

	if ( have_rows( 'page_intro', $field_page_id ) ) { ?>
	<section class="page-intro--page">
		<div class="container">		
			<div class="row d-flex align-items-center">
	    <?php while ( have_rows( 'page_intro', $field_page_id ) ) { the_row();     	
	        $page_intro_image = get_sub_field( 'page_intro_image' );

	        if ( is_single() || is_singular( 'soul_healing_event' ) || is_page_template( 'page-templates/template-for-clients-recordings.php' ) ) {	        	
	        	$large_title = get_the_title();
	        } else {		        	
	        	$large_title = get_sub_field( 'large_title' );
	        }

        	if ( !is_single() ) {
				$small_title = get_sub_field( 'small_title' );        		
        	}
        ?>
        		<?php if ( !empty( $small_title ) || !empty( $large_title ) ) { ?>
				<div class="col-lg-5 text-center text-lg-left page-intro--page-content">
					<h1><?php if ( !empty( $small_title ) ) {?><span><?php esc_html_e( $small_title, 'tgs_wp' ); ?></span><br><?php } ?> <?php if ( !empty( $large_title ) ) { esc_html_e( $large_title, 'tgs_wp' ); } ?></h1>

					<?php if ( 'post' === get_post_type() && !is_home() ) { ?>
					<div class="entry-meta">
						<?php tgs_wp_posted_on(); ?>
						<a href="/blog/" class="link--solid">‹ Back to Blog</a>
					</div>
					<?php } ?>					

				<?php if ( is_page( 5116 ) || is_page( 'faqs' ) ) { ?>
					<a href="/book-now/" class="button white-hover"><?php esc_html_e( 'Book Now', 'tgs_wp' ); ?></a>
				<?php } ?>
				</div>
				<?php } ?>				

				<?php if ( !empty( $page_intro_image ) ) { ?>
				<div class="col-lg-7 text-center text-lg-left page-intro--page-image">
					<img src="<?php echo esc_url( $page_intro_image['url'] ); ?>" alt="<?php echo esc_attr( $page_intro_image['alt'] ); ?>" class="img-fluid">
				</div>
				<?php } ?>
			        
	    <?php } ?>
	    	</div>
		</div>

    </section>
	<?php } ?>

