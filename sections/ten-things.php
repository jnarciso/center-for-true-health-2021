	<?php if ( have_rows( 'ten_things' ) ) { ?>
    	<section class="practitioner--ten-things">
    		<div class="container">		
	    <?php while ( have_rows('ten_things') ) { the_row();
	    	$ten_things_image = get_sub_field( 'ten_things_image' );
	        $ten_things_small_title = get_sub_field( 'ten_things_small_title' );
	        $ten_things_large_title = get_sub_field( 'ten_things_large_title' );
	        $ten_things_content = get_sub_field( 'ten_things_content' ); 
	    ?>
	        	<div class="row d-flex align-items-center">
	        		<div class="col-lg-5 offset-lg-1 order-lg-2 text-center text-lg-left">
						<h2><?php if ( !empty( $ten_things_small_title ) ) { ?><span><?php esc_html_e( $ten_things_small_title, 'tgs_wp' ); ?></span><br><?php } ?><?php if ( !empty( $ten_things_large_title ) ) { esc_html_e( $ten_things_large_title, 'tgs_wp' ); } ?></h2>

						<?php if ( !empty( $ten_things_content ) ) {
							echo wp_kses_post( $ten_things_content );
						} ?>

						<a href="/practitioners/10-things-nobody-tells-you-about-starting-your-own-business/" class="button"><?php esc_html_e( 'Read More', 'tgs_wp' ); ?></a>
	        		</div>
	        		<?php if ( !empty( $ten_things_image ) ) { ?>
	        		<div class="col-lg-6 order-lg-1 text-center image-column">
	        			<img src="<?php echo esc_url( $ten_things_image['url'] ); ?>" alt="<?php echo esc_attr( $ten_things_image['alt']  ); ?>" class="img-fluid">
	        		</div>
	        		<?php } ?>	        		
	        	</div>
	    <?php } ?>
	    	</div>
	    </section>
	<?php } ?>