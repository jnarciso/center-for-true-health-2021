/*
	TayloeGray - Gulp v2.0 ECMAScript modules (ESM) version.
*/


/*
Declare dependencies
*/
import gulp from 'gulp';
import sass from 'gulp-dart-sass';
import sourcemaps from 'gulp-sourcemaps';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';
import newer from 'gulp-newer';
import terser from 'gulp-terser';
import autoprefixer from 'gulp-autoprefixer';
import notify from 'gulp-notify';
import realFavicon from 'gulp-real-favicon';
import del from 'del';


/*
Declare Gulp constants
*/
const { src, dest, watch, parallel, series } = gulp;


/*
Vendor JS Function - `gulp vScripts`
	Concatenates Scripts in Vendor Folder
	Minifies concatenated file
	Writes sourcemap
	Saves minified file (vendors.min.js)
*/
export const vScripts = () => {
	return src('./includes/js/vendor/*')
    	.pipe(sourcemaps.init())
		.pipe(concat('vendors.min.js'))
    	.pipe(terser())
   		.pipe(sourcemaps.write('./'))
		.pipe(dest('./includes/js'));
}


/*
Custom JS Function - `gulp cScripts`
	Concatenates Scripts in Custom Folder
	Minifies concatenated file
	Writes sourcemap
	Saves minified file (custom.min.js)
*/
export const cScripts = () => {
	return src('./includes/js/custom/*')
    	.pipe(sourcemaps.init())
		.pipe(concat('custom.min.js'))
    	.pipe(terser())
   		.pipe(sourcemaps.write('./'))
		.pipe(dest('./includes/js'));
}


/*
Stylesheet Function - `gulp styles`
	Compiles Sass to CSS
	Minifies file
	Auto-prefixes for browsers based on criteria
	Writes sourcemap
	Saves minified file (styles.min.css)
*/
export const styles = () => {
	return src('./includes/sass/styles.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({ outputStyle: 'compressed', quietDeps: true }).on("error", notify.onError()))
		.pipe(concat('styles.min.css'))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write('.'))
		.pipe(dest('./includes/css'));
}


/*
Image Compression Function - `gulp images`
	Checks for *new* images located in "images/raw"
	Compresses images with nerd magic
	Saves compressed images to "images/processed"
*/
export const images = () => {
	return src('./includes//images/raw/*')
		.pipe(newer('./includes/images/processed'))
		.pipe(imagemin())
		.pipe(dest('./includes/images/processed'))
}


/*
Image Clean Function - `gulp cleanImages`
	Deletes the *images/processed* folder
*/
export const cleanImages = () => {
	return del('./includes/images/processed', { force: true })
}


/*
Favicon Generate Function - `gulp favicon`
	Generates an assortment of favicons based on the masterPicture
	Saves generated images in the dest location *images/device-icons*
*/
export const favicon = (done) => {
	realFavicon.generateFavicon({
		masterPicture: './includes/images/favicon.png',
		dest: './includes/images/device-icons',
		iconsPath: '/',
		design: {
			ios: {
				pictureAspect: 'noChange',
				assets: {
					ios6AndPriorIcons: false,
					ios7AndLaterIcons: false,
					precomposedIcons: false,
					declareOnlyDefaultIcon: true
				}
			},
			desktopBrowser: {
				design: 'raw'
			},
			windows: {
				pictureAspect: 'noChange',
				backgroundColor: '#da532c',
				onConflict: 'override',
				assets: {
					windows80Ie10Tile: false,
					windows10Ie11EdgeTiles: {
						small: false,
						medium: true,
						big: false,
						rectangle: false
					}
				}
			},
			androidChrome: {
				pictureAspect: 'noChange',
				themeColor: '#ffffff',
				manifest: {
					display: 'standalone',
					orientation: 'notSet',
					onConflict: 'override',
					declared: true
				},
				assets: {
					legacyIcon: false,
					lowResolutionIcons: false
				}
			},
			safariPinnedTab: {
				pictureAspect: 'silhouette',
				themeColor: '#5bbad5'
			}
		},
		settings: {
			scalingAlgorithm: 'Mitchell',
			errorOnImageTooSmall: false,
			readmeFile: false,
			htmlCodeFile: false,
			usePathAsIs: false
		},
		markupFile: 'faviconData.json',
	}, function() {
		done();
	});
} 


/*
Favicon Clean Function - `gulp cleanImages`
	Deletes the *images/device-icons* folder
*/
export const cleanFavicon = () => {
	return del('./includes/images/device-icons', { force: true })
}


/*
Change watcher function - `gulp watcher`
	Watches for changes to styles and scripts.
	Runs appropriate function on change.
*/
export const watcher = () => {
	watch('./includes/sass/**/*', styles);
	watch('./includes/js/vendor/*.js', vScripts);
	watch('./includes/js/custom/*.js', cScripts);
	return;
}


/*
Default - `gulp`
	Runs styles, vendor scripts, and custom scripts functions and starts the watcher
*/
export default parallel(styles, vScripts, cScripts, watcher);
