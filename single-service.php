<?php
/**
 * The Template for displaying all single Service posts.
 *
 * @package tgs_wp
 */

get_header(); ?>

	<div class="main-content no-intro" id="main-content" role="main">

		<?php if ( have_rows( 'service_intro' ) ) { ?>
		<section class="page-intro--page-plain">	
			<div class="container">		
    			<div class="row d-flex align-items-center">				
			<?php while( have_rows( 'service_intro' ) ) { the_row(); 
				$intro_page_title = get_sub_field( 'alt_service_title' );	
				$intro_content = get_sub_field( 'intro_content' );						
				$intro_photo = get_sub_field( 'intro_photo' );

				if ( !empty($intro_page_title)  ) {
					$service_title = $intro_page_title;
				} else {
					$service_title = get_the_title();
				}				
			?>
					<div class="col-lg-5">
						<div class="row d-flex align-items-center service-row--title">
							<?php if ( has_post_thumbnail() ) { 
								$col_intro_title = 'col-9';
							?>
							<div class="col-3">
								<?php the_post_thumbnail( '', array('class' => 'img-fluid') ); ?>
							</div>
							<?php } else {
								$col_intro_title = 'col-12';
							} ?> 
							<div class="<?php esc_html_e( $col_intro_title ); ?> text-left">
								<h1><?php esc_html_e( $service_title, 'tgs_wp' ); ?></h1>
							</div>
						</div>
						<?php if ( !empty( $intro_content ) ) {
							echo wp_kses_post( $intro_content, 'tgs_wp' );
						} ?>
					</div>    				
        			<div class="col-lg-6 offset-lg-1 page-intro--page-plain--image text-center">
						<img src="<?php echo esc_url( $intro_photo['url'] ); ?>" alt="<?php echo esc_attr( $intro_photo['alt'] ); ?>" class="img-fluid">
	        		</div>
		    <?php } // end while ?>
        		</div>		    
			</div>

			<?php //get_template_part( 'sections/book-appointment' ); ?>

		</section>        		    
		<?php } // end if ?>

		<section class="page-content--container" id="page-content--container">
			<div class="container">
    	<?php 
    		// check if we want to add jump links to this page
    		// build our links to jump to hashed sections below
    		if ( get_field( 'add_jump_links' ) == 'yes' ) { 

    			// check for jump link intro content
    			if ( have_rows( 'jump_links_intro_content' ) ) {
    				while ( have_rows('jump_links_intro_content' ) ) { the_row(); 
    				$jump_links_intro_title = get_sub_field( 'jump_links_intro_title' );
    				$jump_links_intro_content = get_sub_field( 'jump_links_intro_content' );	
    			?>
	        	<section class="row d-flex justify-content-center jump-link-intro-content">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $jump_links_intro_title ) ) { ?>
	        			<h2><?php esc_html_e( $jump_links_intro_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $jump_links_intro_content ) ) { 
	        				echo wp_kses_post( $jump_links_intro_content ); 
	        			} ?>	        			
	        		</div>
	        	</section>
    				<?php } 
    			} ?>

    		<?php if ( have_rows( 'service_content' ) ) { ?>
    			<section class="row d-flex justify-content-center jump-link-section service-content--row">
    				<div class="col-lg-8">
    					<ul class="plain-list">
				    <?php while ( have_rows('service_content') ) { the_row(); 
				        $service_section_title = get_sub_field( 'service_section_title' );	    	
						$hash_link = str_replace(' ', '-', $service_section_title); // Replaces all spaces with hyphens
						$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
						$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
						$hash_link = strtolower( $hash_link ); // Lowercase the string
				   	?>
				    		<li><a href="#<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $service_section_title ); ?></a></li>
			    	<?php } ?>
			    		</ul>
    				</div>
    			</section>
			<?php } ?>    				
    	<? } ?>

    	<?php reset_rows(); ?>

	<?php if ( have_rows( 'service_content' ) ) { ?>
	    <?php while ( have_rows( 'service_content' ) ) { the_row();
	        $service_section_title = get_sub_field( 'service_section_title' );	    	
			$hash_link = str_replace(' ', '-', $service_section_title); // Replaces all spaces with hyphens
			$hash_link = preg_replace('/[^A-Za-z0-9\-]/', '', $hash_link); // Removes special chars
			$hash_link = preg_replace('/-+/', '-', $hash_link); // Replaces multiple hyphens with single one
			$hash_link = strtolower( $hash_link ); // Lowercase the string
	        $service_section_content = get_sub_field( 'service_section_content' ); ?>

	        	<section class="row d-flex justify-content-center service-content--row">
	        		<div class="col-lg-8">
	        			<?php if ( !empty( $service_section_title ) ) { ?>
	        			<h2 id="<?php esc_html_e( $hash_link ); ?>"><?php esc_html_e( $service_section_title ); ?></h2>
	        			<?php } ?>

	        			<?php if ( !empty( $service_section_content ) ) { 
	        				echo wp_kses_post( $service_section_content ); 
	        			} ?>	        			
	        		</div>
	        	</section>
	    <?php } ?>	
	<? } ?>
		</div>
	</section>

	<?php get_template_part('sections/slider'); ?>		

	</div>

<?php 
	get_footer();
